create table person
(
id integer not null,
name varchar(255) not null,
location varchar(255),
birth_date timestamp,
primary key(id)

);

insert into person (ID,NAME,LOCATION,BIRTH_DATE)
values (1001,'vicky','chennai',sysdate());

insert into person (ID,NAME,LOCATION,BIRTH_DATE)
values (1002,'kala','teni',sysdate());

insert into person (ID,NAME,LOCATION,BIRTH_DATE)
values (1003,'meera','erode',sysdate());

insert into person (ID,NAME,LOCATION,BIRTH_DATE)
values (1004,'thandabani','palani',sysdate());
